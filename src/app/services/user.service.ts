/**
 * Created by Maryem on 28/07/2017.
 */
import { Injectable } from '@angular/core';
import { User } from '../classes/user';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable} from 'angularfire2/database';

@Injectable()
export class UserService {


  constructor(private db: AngularFireDatabase) {
  }

  recupererUtilisateurDeBDD(idUtilisateur: string): Promise<User> {
    return new Promise((user) => {
        this.db.object(idUtilisateur).subscribe(data => {
          user(data);
        });
    })
  };

  recupererListeClients(): Promise<FirebaseListObservable<User[]>> {
    return new Promise((users)=>{
      this.db.list('users').subscribe(data => {
        users(data);
      })
    })
  }

  utilisateurEsrAdmin(idUtilisateur: string): Promise<boolean> {
    return new Promise((isAdmin) => {
      this.db.object('admins/'+idUtilisateur).subscribe(data => {
        isAdmin(data.mail != undefined);
      });
    })
  };

}
