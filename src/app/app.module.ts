import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import {ConnexionComponent} from './connexion/connexion.component';
import {AcceuilComponent} from './acceuil/acceuil.component';
import {MenuComponent} from './menu/menu.component';
import {TitreComponent} from './titre/titre.component';
import {EnvoyesComponent} from './mails/envoyes.component';
import {NouveauComponent} from './mails/nouveau.component';
import {ReceptionsComponent} from './mails/receptions.component';
import {BaseclientComponent} from './baseclient/baseclient.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireAuth } from 'angularfire2/auth';
import {UserService} from './services/user.service';
import {AngularFireDatabase} from "angularfire2/database";
import { CookieModule } from 'ngx-cookie';
import {AccessGuard} from "./services/auth-guard.service";

// Must export the config
export const firebaseConfig = {
  apiKey: 'AIzaSyAAfZwceMWznmUOM0tDeweIbB751DTeeDw',
  authDomain: 'yumemirai-e65fc.firebaseapp.com',
  databaseURL: 'https://yumemirai-e65fc.firebaseio.com',
  projectId: 'yumemirai-e65fc',
  storageBucket: 'yumemirai-e65fc.appspot.com',
  messagingSenderId: '802174248681'
};


@NgModule({
  declarations: [
    AppComponent,
    ConnexionComponent,
    AcceuilComponent,
    MenuComponent,
    TitreComponent,
    EnvoyesComponent,
    NouveauComponent,
    ReceptionsComponent,
    BaseclientComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    CookieModule.forRoot()
  ],
  providers: [
    AngularFireAuth,
    AngularFireDatabase,
    UserService,
    AccessGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
