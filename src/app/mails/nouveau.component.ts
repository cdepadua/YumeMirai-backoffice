import { Component } from '@angular/core';

/* */
@Component({
  selector: 'nouveau-component', /*si on trouve une balise nomée connexion-component
   on la remplace par le fichier html en dessous et son code css
   */
  templateUrl: './nouveau.component.html',
  styleUrls: ['./nouveau.component.css']
})
/* */
export class NouveauComponent {
}
