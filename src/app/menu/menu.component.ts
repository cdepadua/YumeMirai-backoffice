import { Component } from '@angular/core'; /*importer la balise component pour creer la page html*/

/* */
@Component({
  selector: 'menu-component', /*si on trouve une balise nomée connexion-component
   on la remplace par le fichier html en dessous et son code css
   */
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
/* */
export class MenuComponent {
  test = 'menu';
}

