/**
 * Created by Maryem on 28/07/2017.
 */
export class User {
  id: string;
  mail: string;
  nom: string;
  prenom: string;
  telephone: string;
}
