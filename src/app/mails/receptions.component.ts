import { Component } from '@angular/core'; /*importer la balise component pour creer la page html*/

/* */
@Component({
  selector: 'receptions-component', /*si on trouve une balise nomée connexion-component
   on la remplace par le fichier html en dessous et son code css
   */
  templateUrl: './receptions.component.html',
  styleUrls: ['./receptions.component.css']
})
/* */
export class ReceptionsComponent {
  test = 'receptions';
}
