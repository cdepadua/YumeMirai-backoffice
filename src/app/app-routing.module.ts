import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { BaseclientComponent } from './baseclient/baseclient.component';
import {AccessGuard} from "./services/auth-guard.service";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/clients',
    pathMatch: 'full'
  },
  {
    path: 'connexion',
    component: ConnexionComponent
  },
  {
    path : 'clients',
    component : BaseclientComponent,
    data: { requiresLogin: true },
    canActivate: [ AccessGuard ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
