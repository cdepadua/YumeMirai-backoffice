/**
 * Created by cesar on 30/07/2017.
 */
import  { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, Router} from "@angular/router";
import { Observable } from "rxjs/Rx";
import {UserService} from "./user.service";
import {CookieService} from "ngx-cookie/index";

@Injectable()
export class AccessGuard implements CanActivate {

  constructor ( private userService: UserService,
                private _coockieService: CookieService,
                private  router: Router ){}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean>|Promise<boolean>|boolean {
    const requiresLogin = route.data.requiresLogin || false;
    if (requiresLogin) {
      const token = this._coockieService.get('authToken');
      if(token){
        this.userService.utilisateurEsrAdmin(token).then(reponse => {
          if(!reponse) {
            this.router.navigateByUrl('connexion');
          }
        }).catch(err => {
          this.router.navigateByUrl('connexion');
          console.log(err);
        });
      }else{
        this.router.navigateByUrl('connexion');
      }
    }
    return true;
  }
}

