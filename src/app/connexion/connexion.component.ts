import { Component } from '@angular/core';/*importer la balise component pour creer la page html*/
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {UserService} from '../services/user.service';
import {CookieModule, CookieService} from 'ngx-cookie';

/* */
@Component({
  selector: 'connexion-component', /*si on trouve une balise nomée connexion-component
    on la remplace par le fichier html en dessous et son code css
  */
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
/* */
export class ConnexionComponent {
  formulaireConnexion: FormGroup;
  user: Observable<firebase.User>;

  constructor(fb: FormBuilder,
              public afAuth: AngularFireAuth,
              private router: Router,
              private userService: UserService,
              private _cookieService: CookieService) {
    this.formulaireConnexion = fb.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      motDePasse: [null, Validators.compose([Validators.required, Validators.minLength(8)])]
    });
    this.user = afAuth.authState;
  }
  onSubmit(value: any)  {
    const that = this;
    this.afAuth.auth.signInWithEmailAndPassword(value.email, value.motDePasse)
      .then(function(user){
        that.userService.utilisateurEsrAdmin(user.uid).then(reponse=> {
          if(reponse) {
            console.log('admin');
            that._cookieService.put('authToken', user.uid);
            that.router.navigateByUrl('clients');
          }else{
            alert('You\'r not an admin');
          }
        });
      })
      .catch(function(error) {
        // Handle Errors here.
        alert(error.message);
        console.log(error);
      });
  }
}

