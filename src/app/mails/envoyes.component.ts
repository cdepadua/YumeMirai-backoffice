import { Component } from '@angular/core';

/* */
@Component({
  selector: 'envoyes-component', /*si on trouve une balise nomée connexion-component
   on la remplace par le fichier html en dessous et son code css
   */
  templateUrl: './envoyes.component.html',
  styleUrls: ['./envoyes.component.css']
})
/* */
export class EnvoyesComponent {
  test = 'msg envoyés';
}
