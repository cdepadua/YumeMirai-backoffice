import { Component } from '@angular/core'; /*importer la balise component pour creer la page html*/
import { FirebaseListObservable} from 'angularfire2/database';
import {UserService} from "../services/user.service";
import {User} from "../classes/user";

/* */
@Component({
  selector: 'baseclient-component', /*si on trouve une balise nomée connexion-component
   on la remplace par le fichier html en dessous et son code css
   */
  templateUrl: './baseclient.component.html',
  styleUrls: ['./baseclient.component.css']
})
/* */
export class BaseclientComponent {
  users: FirebaseListObservable<User[]>;
  constructor(private userService: UserService) {
    const that = this;
    userService.recupererListeClients().then(clients => {
      that.users = clients;
      console.log(that.users);
    });
  }
}
